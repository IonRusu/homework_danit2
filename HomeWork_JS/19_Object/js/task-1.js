// 1. Створіть об'єкт product з властивостями name, price та discount. Додайте метод для виведення повної ціни товару з урахуванням знижки. Викличте цей метод та результат виведіть в консоль.
 
function productCard(name, price, discount) {
    const product = {
        name,
        price,
        discount,

        discountPrice: function () {
            return this.price - (this.price * this.discount / 100)
        }
    }
    console.log(`Discounted price: ${product.discountPrice()} UAH`);
}
productCard('Iphone', 50000, 25)