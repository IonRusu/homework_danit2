// 3. Створіть функцію, яка визначає скільки повних років користувачу. Отримайте дату народження користувача через prompt. Функція повина повертати значення повних років на дату виклику функцію.

function getDate() {
    let userBirthDate;
    let birthDate;

    do {
        userBirthDate = prompt('Введіть вашу дату народження у форматі рік-місяць-день', '1998-02-11');
        if (!userBirthDate) {
            return 'Ви вказали не правильно вашу дату. Вкажіть ще раз', '1998-12-13';
        }
        birthDate = new Date(userBirthDate);
    } while (isNaN(birthDate));

    let today = new Date(); 
    
    let clcAge = today.getFullYear() - birthDate.getFullYear()
    let clcMonth = today.getMonth() - birthDate.getMonth()
    let clcDay = today.getDate() - birthDate.getDate()

    if (clcMonth < 0 || (clcMonth === 0 && clcDay < 0 )) {
        clcAge--
    }
    
    return clcAge
}

console.log(`Ваш вік ${getDate()} років`);