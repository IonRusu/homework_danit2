// 2. Створіть функцію, яка перевіряє довжину рядка. Вона приймає рядок, який потрібно перевірити, максимальну довжину і повертає true, якщо рядок менше або дорівнює вказаній довжині, і false, якщо рядок довший. Ця функція стане в нагоді для валідації форми. Приклади використання функції:

// Рядок коротше 20 символів
// funcName('checked string', 20); // true

// Довжина рядка дорівнює 18 символів
// funcName('checked string', 10); // false


function checkString(str, maxLength) {
    return str.length <= maxLength;
}

console.log(checkString('checked string', 20));
console.log(checkString('checked string', 10));