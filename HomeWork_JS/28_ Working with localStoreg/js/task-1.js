// Технічні вимоги:
// - Взяти готове домашнє завдання HW-4 "Price cards" з блоку Basic HMTL/CSS.
// - Додати на макеті кнопку "Змінити тему".
// - При натисканні на кнопку - змінювати колірну гаму сайту (кольори кнопок, фону тощо) на ваш розсуд. При повторному натискання - повертати все як було спочатку - начебто для сторінки доступні дві колірні теми.
// - Вибрана тема повинна зберігатися після перезавантаження сторінки.

// Примітки:
// - при виконанні завдання перебирати та задавати стилі всім елементам за допомогою js буде вважатись помилкою;
// - зміна інших стилів сторінки, окрім кольорів буде вважатись помилкою.




const button = document.getElementById('theme');
const body = document.querySelector('body')


const savedTheme = localStorage.getItem('theme');
if (savedTheme === 'dark') {
    body.classList.add('dark-theme');
}

button.addEventListener('click', () => {
    body.classList.toggle('dark-theme');
    let currentTheme;
    if (body.classList.contains('dark-theme')) {
        currentTheme = 'dark';
    } else {
    currentTheme = 'light';
    }   
    localStorage.setItem('theme', currentTheme);
});