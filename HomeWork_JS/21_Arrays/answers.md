<!-- Теоретичні питання -->
1. Опишіть своїми словами як працює метод forEach.

    Метод forEach перебирає елементи обʼєкта один раз 

2. Які методи для роботи з масивом мутують існуючий масив, а які повертають новий масив? Наведіть приклади.

        Методи які мутую:
    push — додає елементи в кінець масиву
    pop — видаляє останній елемент
    unshift — додає елементи на початок
    shift — видаляє перший елемент
    splice — видаляє або замінює елементи
    reverse — змінює порядок елементів зеркально

        Методи які не мутують і повертають новий масив:
    map — створює новий масив, виконуючи функцію для кожного елемента
    filter — повертає новий масив із елементами, які пройшли умову
    slice — повертає копію частини масиву
    concat — об’єднує масиви
    reduce — акумулює значення

3. Як можна перевірити, що та чи інша змінна є масивом?

    Для перевірки використовується метод Array.isArray() 

4. В яких випадках краще використовувати метод map(), а в яких forEach()?

    map:Використовується коли потрібно створити новий масив, перетворюючи елементи вихідного масиву.

    forEach: використовується коли потрібно просто виконати дію для кожного елемента масиву