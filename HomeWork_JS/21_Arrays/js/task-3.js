// 3. Реалізувати функцію фільтру масиву за вказаним типом даних. (Опціональне завдання)

function filterArrayType(array, type) {
    const result = [];
    for (let i = 0; i < array.length; i++) {
      if (typeof array[i] === type) {
        result.push(array[i]);
      }
    }
    return result;
  }
const randomArray = [1,'string', 23, true, 12, false, "word",]

console.log(filterArrayType(randomArray, 'number'));
console.log(filterArrayType(randomArray, 'string'));
console.log(filterArrayType(randomArray, "boolean"));
