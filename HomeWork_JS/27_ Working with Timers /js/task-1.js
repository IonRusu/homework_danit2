// Практичне завдання 1:
// -Створіть HTML-файл із кнопкою та елементом div.
// -При натисканні кнопки використовуйте setTimeout, щоб змінити текстовий вміст елемента div через затримку 3 секунди. Новий текст повинен вказувати, що операція виконана успішно.

const btn = document.createElement('button')
btn.textContent = 'Натисніть тут'
document.body.append(btn)
const div = document.createElement('div')
div.textContent = "Чекаю на дію..."
document.body.append(div)

btn.addEventListener('click', () =>{
    div.textContent = 'Обробка..'
    setTimeout(() =>{
        div.textContent = 'Операція виконана успішно'
    },3000)
})


