// Практичне завдання 2:
// Реалізуйте таймер зворотного відліку, використовуючи setInterval. При завантаженні сторінки виведіть зворотний відлік від 10 до 1 в елементі div. Після досягнення 1 змініть текст на "Зворотній відлік завершено".



const div = document.createElement('div')
document.body.append(div)
div.textContent = 10
div.style.fontSize = '28px'
let counter = '10'

const timer = setInterval(() =>{
    counter--
    div.textContent = counter

    if (counter <= 0) {
        clearInterval(timer)
        div.textContent = "Зворотній відлік завершено"
    }
},1000)

