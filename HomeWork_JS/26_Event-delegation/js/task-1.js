// Практичне завдання:
// Реалізувати перемикання вкладок (таби) на чистому Javascript.

// Технічні вимоги:
// - У папці tabs лежить розмітка для вкладок. Потрібно, щоб після натискання на вкладку відображався конкретний текст для потрібної вкладки. При цьому решта тексту повинна бути прихована. У коментарях зазначено, який текст має відображатися для якої вкладки.
// - Розмітку можна змінювати, додавати потрібні класи, ID, атрибути, теги.
// - Потрібно передбачити, що текст на вкладках може змінюватись, і що вкладки можуть додаватися та видалятися. При цьому потрібно, щоб функція, написана в джаваскрипті, через такі правки не переставала працювати.

// Умови:
// - При реалізації обов'язково використовуйте прийом делегування подій (на весь скрипт обробник подій повинен бути один).



const tabsContainer = document.querySelector(".tabs");
const contents = document.querySelectorAll(".tab-content"); 

document.addEventListener('DOMContentLoaded', () =>{
  tabsContainer.addEventListener('click', (event) =>{
    const clickedTab = event.target
    if (clickedTab.classList.contains('tabs-title')) {
      tabsContainer.querySelectorAll('.tabs-title').forEach(tab => tab.classList.remove('active'))
      contents.forEach(content => content.classList.remove('active'))

      clickedTab.classList.add('active')

      const tabName = clickedTab.dataset.tab
      const correspondingContent = document.querySelector(`.tab-content[data-tab='${tabName}']`)

      if (correspondingContent) {
        correspondingContent.classList.add('active')
      }
    }
  })
})