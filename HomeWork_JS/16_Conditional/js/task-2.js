


let month = prompt("Введіть назву місяця українською мовою").toLowerCase();
let days;

switch (month) {
    case "січень":
    case "березень":
    case "травень":
    case "липень":
    case "серпень":
    case "жовтень":
    case "грудень":
        days = 31;
        break;
    case "квітень":
    case "червень":
    case "вересень":
    case "листопад":
        days = 30;
        break;
    case "лютий":
        days = 28; 
        break;
    default:
        days = "Можливо, ви ввели неправильну назву місяця.";
        break;
}

console.log(month + " " + days);
