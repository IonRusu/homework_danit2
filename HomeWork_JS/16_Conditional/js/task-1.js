// 1. Попросіть користувача ввести свій вік за допомогою prompt.
// Якщо вік менше 12 років, виведіть у вікні alert повідомлення про те,
// що він є дитиною, якщо вік менше 18 років, виведіть повідомлення про те,
// що він є підлітком, інакше виведіть повідомлення про те, що він є дорослим.

let userAge = parseInt(prompt('How old are you'));

if (!!userAge){
    if (userAge <= 12) {
        alert('You are a child')
    }else if (userAge <= 18){
        alert('You are a teenager')
    }else if (userAge >= 18){
        alert('You are an adult')
    }
}else{
    console.log('Enter a number')
}
