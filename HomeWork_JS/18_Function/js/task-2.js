"use strict";
//! 2. Завдання: Реалізувати функцію, яка виконуватиме математичні операції з введеними користувачем числами.


function calculate(num1, num2, operator) {
    switch (operator) {
        case '+':
            return num1 + num2;
        case '-':
            return num1 - num2;
        case '*':
            return num1 * num2;
        case '/':
            if (num2 === 0) {
                return "Ділення на нуль не дозволене!";
            }
            return num1 / num2;;
        default:
            break;
    }
}
const number1 = +prompt('Введіть перше чило')
const number2 = +prompt('Введіть друге чило')
const operator = prompt('Виберіть оператор: + , - , * , / ')

const rezult = calculate(number1, number2, operator);

console.log('Результат: ' + rezult);
alert('Результат: ' + rezult);
