// Практичні завдання
// 1. Запитайте у користувача два числа. Перевірте, чи є кожне з введених значень числом. Якщо ні, то запитуйте у користувача нове занчення до тих пір, поки воно не буде числом. Виведіть на екран всі числа від меншого до більшого за допомогою циклу for.

let firstNum;
let secondNum 


do {
    firstNum = prompt('вкажіть перше число');
} while (isNaN(firstNum));


do {
    secondNum = prompt('вкажіть друге число');
} while (isNaN(secondNum));

let min = Math.min(firstNum, secondNum);
let max = Math.max(firstNum, secondNum);

for (let i = min; i <= max ; i++) {
    console.log(i);
}