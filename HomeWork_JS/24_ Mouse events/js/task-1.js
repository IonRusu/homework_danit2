// 1. Додати новий абзац по кліку на кнопку:
// По кліку на кнопку <button id="btn-click">Click Me</button>, створіть новий елемент <p> з текстом "New Paragraph" і додайте його до розділу <section id="content">

const btn = document.getElementById('btn-click')
const sectionContent = document.getElementById('content')
btn.addEventListener('click', () =>{
    const newParagraf = document.createElement('p')
    newParagraf.textContent = "New Paragraph"

    sectionContent.insertAdjacentElement('afterbegin', newParagraf)
})





