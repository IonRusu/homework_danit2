// 2. Додати новий елемент форми із атрибутами:
// Створіть кнопку з id "btn-input-create", додайте її на сторінку в section перед footer.
// По кліку на створену кнопку, створіть новий елемент <input> і додайте до нього власні атрибути, наприклад, type, placeholder, і name. та додайте його під кнопкою.


const sectionContent = document.getElementById('content')

const newBtn = document.createElement('button')
newBtn.id = "btn-input-create"
newBtn.textContent = 'New input'
newBtn.style.padding = '8px 15px'
sectionContent.appendChild(newBtn)

const form = document.createElement('form')
sectionContent.appendChild(form);


newBtn.addEventListener('click', () =>{
    const input = document.createElement('input')
    input.type = 'text';
    input.placeholder = 'Enter your text here';
    input.name = 'gogi';
    form.appendChild(input)
})

