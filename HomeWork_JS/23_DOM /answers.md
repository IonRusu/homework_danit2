<!-- Теоретичні питання -->
1. Які способи JavaScript можна використовувати для створення та додавання нових DOM-елементів?

Створення елементa document.createElement
Додавання HTML коду через innerHTML
Методи append, prepend, appendChild 

2. Опишіть покроково процес видалення одного елементу (умовно клас "navigation") зі сторінки.

За допомогою методу querySelector знаходимо елемент потім шукаєм батківського елемента parent і же тоді parent.removeChild(navigation) або navigation.remuve()

3. Які є методи для вставки DOM-елементів перед/після іншого DOM-елемента?

Методи append (в кінці), prepend (на початку), appendChild (додає дочірній елемент)
Вставити елемент відносно існуючого insertAdjacentHTML
beforebegin — перед елементом.
afterbegin — на початку елемента.
beforeend — в кінці елемента.
afterend — після елемента.
