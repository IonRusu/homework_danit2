// 2. Створіть новий елемент <select>. Задайте йому ідентифікатор "rating", і додайте його в тег main перед секцією "Features".

// Створіть новий елемент <option> зі значенням "4" і текстом "4 Stars", і додайте його до списку вибору рейтингу.
// Створіть новий елемент <option> зі значенням "3" і текстом "3 Stars", і додайте його до списку вибору рейтингу.
// Створіть новий елемент <option> зі значенням "2" і текстом "2 Stars", і додайте його до списку вибору рейтингу.
// Створіть новий елемент <option> зі значенням "1" і текстом "1 Star", і додайте його до списку вибору рейтингу.

const main = document.querySelector('.main')
const sectionFeatures = document.querySelector('.features')

const select = document.createElement('select')
select.id = 'rating'

const optionsInfo = [
    { value: '4', text: '4 Stars' },
    { value: '3', text: '3 Stars' },
    { value: '2', text: '2 Stars' },
    { value: '1', text: '1 Star' }
];

for (let i = 0; i < optionsInfo.length; i++) {
    const option = document.createElement('option')
    option.value = optionsInfo[i].value 
    option.textContent = optionsInfo[i].text 
    select.appendChild(option)
}

sectionFeatures.insertAdjacentElement('beforebegin',select)

select.style.marginInline = '50%'




