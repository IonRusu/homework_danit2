// 1. Створити клас Employee, у якому будуть такі характеристики - name (ім'я), age (вік), salary (зарплата). Зробіть так, щоб ці характеристики заповнювалися під час створення об'єкта.
// 2. Створіть гетери та сеттери для цих властивостей.
// 3. Створіть клас Programmer, який успадковуватиметься від класу Employee, і який матиме властивість lang (список мов).
// 4. Для класу Programmer перезапишіть гетер для властивості salary. Нехай він повертає властивість salary, помножену на 3.
// 5. Створіть кілька екземплярів об'єкта Programmer, виведіть їх у консоль.



class Employee {
    constructor(name, age, salary){
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    get name() {
        return this._name;
    }
    get age() {
        return this._age;
    }
    get salary() {
        return this._salary;
    }

    set name(value) {
        this._name = value;
    }
    set age(value) {
        this._age = value;
    }
    set salary(value) {
        this._salary = value
    }
}


class Programmer extends Employee {
    constructor(name, age, salary, lang){
        super(name, age, salary)
        this.lang = lang
    }
    get salary() {
        return super.salary * 3
    }
}

const programmer1 = new Programmer("Ivan", 25, 4000, ["Python", "JavaScript"]);
const programmer2 = new Programmer("Vasya", 30, 5000, ["Java", "C++"]);

console.log(`Programmer 1: 
    Name: ${programmer1.name}, 
    Age: ${programmer1.age}, 
    Salary: ${programmer1.salary}$, 
    Languages: ${programmer1.lang.join(", ")}`);
    
console.log(`Programmer 2: 
    Name: ${programmer2.name}, 
    Age: ${programmer2.age}, 
    Salary: ${programmer2.salary}$, 
    Languages: ${programmer2.lang.join(", ")}`);

