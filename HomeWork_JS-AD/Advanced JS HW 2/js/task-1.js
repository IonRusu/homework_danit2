const books = [
  {
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70,
  },
  {
    author: "Сюзанна Кларк",
    name: "Джонатан Стрейндж і м-р Норрелл",
  },
  {
    name: "Дизайн. Книга для недизайнерів.",
    price: 70,
  },
  {
    author: "Алан Мур",
    name: "Неономікон",
    price: 70,
  },
  {
    author: "Террі Пратчетт",
    name: "Рухомі картинки",
    price: 40,
  },
  {
    author: "Анґус Гайленд",
    name: "Коти в мистецтві",
  },
];

class Books {
  constructor(books, rootId) {
    this.books = books;
    this.rootId = document.getElementById(rootId);
  }

  render() {
    const ul = document.createElement("ul");
    this.books.forEach((book) => {
      if (this.validateBook(book)) {
        const li = document.createElement("li");
        li.textContent = `${book.author} - "${book.name}" (Ціна: ${book.price} грн)`;
        ul.appendChild(li);
      }
    });
    this.rootId.appendChild(ul);
  }

  validateBook(book) {
    try {
      if (!book.author) {
        throw new Error(
          `Відсутній автор у книзі "${book.name}"`
        );
      }
      if (!book.name) {
        throw new Error("Відсутня назва книги");
      }
      if (!book.price) {
        throw new Error(`Відсутня ціна у книги "${book.name}"`);
      }
      return true;
    } catch (error) {
      console.error(`Помилка: ${error.message}`);
    }
  }
}

const booksList = new Books(books, "root");
booksList.render();
