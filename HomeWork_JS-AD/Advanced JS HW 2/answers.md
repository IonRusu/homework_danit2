<!-- Теоретичне питання -->
1. Наведіть кілька прикладів, коли доречно використовувати в коді конструкцію try...catch.

	Обробка помилки при діленні на нуль:

try {
  let result = 10 / 0;
} catch (error) {
  console.error("Не можна ділити на нуль");
}

    Обробка помилки при парсингу некоректного JSON:

try {
  const data = JSON.parse('{"name": "Іван",}');
} catch (error) {
  console.error("Некоректний JSON");
}