//! Завдання
// Отримати список фільмів серії Зоряні війни та вивести на екран список персонажів для кожного з них.

const url = "https://ajax.test-danit.com/api/swapi/films";

const container = document.getElementById("films");

axios
  .get(url)
  .then(({ data }) => {
    console.log(data);

    data.sort((a, b) => a.episodeId - b.episodeId);
    data.forEach(({ name, episodeId, openingCrawl, characters }) => {
      const filmCard = document.createElement("div");
      filmCard.classList.add("film-card");

      filmCard.innerHTML = `
        <p class="film-name"> <span>${episodeId}</span> ${name}</p>
        <ul class="film-actors"></ul>
        <p class="films-discription">${openingCrawl}</p>
      `;

      const charactersList = filmCard.querySelector(".film-actors");

      characters.forEach((characterUrl) => {
        axios
          .get(characterUrl)
          .then(({ data }) => {
            const characterItem = document.createElement("li");
            characterItem.textContent = data.name;
            charactersList.appendChild(characterItem);
          })
          .catch((err) => {
            console.warn("Помилка завантаження персонажа:", err);
          });
      });

      container.appendChild(filmCard);
    });
  })
  .catch((err) => {
    console.warn("Помилка завантаження фільмів:", err);
  });
