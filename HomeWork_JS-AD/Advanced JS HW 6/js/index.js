//! Завдання
// Написати програму "Я тебе знайду по IP"

//? Технічні вимоги:
// Створити просту HTML-сторінку з кнопкою Знайти по IP.
// Натиснувши кнопку - надіслати AJAX запит за адресою https://api.ipify.org/?format=json, отримати звідти IP адресу клієнта.
// Дізнавшись IP адресу, надіслати запит на сервіс https://ip-api.com/ та отримати інформацію про фізичну адресу.
// під кнопкою вивести на сторінку інформацію, отриману з останнього запиту – континент, країна, регіон, місто, район.
// Усі запити на сервер необхідно виконати за допомогою async await



document.getElementById("findBtn").addEventListener("click", async function () {
  try {
    const ipResponse = await axios.get("https://api.ipify.org/?format=json");

    const locationResponse = await axios.get(`http://ip-api.com/json`);
    const locationData = locationResponse.data;
    console.log(locationData);

    document.getElementById("result").innerHTML = `
          <p><strong>Континент:</strong> ${locationData.timezone.split("/")[0] || "Немає даних"}</p>
          <p><strong>Країна:</strong> ${locationData.country || "Немає даних"}</p>
          <p><strong>Регіон:</strong> ${locationData.regionName || "Немає даних"}</p>
          <p><strong>Місто:</strong> ${locationData.city || "Немає даних"}</p>
          <p><strong>Район:</strong> ${locationData.region || "Немає даних"}</p>
      `;
  } catch (error) {
    console.error("Помилка отримання даних:", error);
    document.getElementById("result").innerHTML =
      '<p style="color: red;">Не вдалося отримати інформацію</p>';
  }
});
