/*
   {
    "id": 1,
    "name": "Leanne Graham",
    "username": "Bret",
    "email": "Sincere@april.biz",
    "address": {
      "street": "Kulas Light",
      "suite": "Apt. 556",
      "city": "Gwenborough",
      "zipcode": "92998-3874",
      "geo": {
        "lat": "-37.3159",
        "lng": "81.1496"
      }
    },
    "phone": "1-770-736-8031 x56442",
    "website": "hildegard.org",
    "company": {
      "name": "Romaguera-Crona",
      "catchPhrase": "Багаторівнева клієнт-серверна нейронна мережа",
      "bs": "використовувати електронні ринки в реальному часі"
    }
  }
*/
/*
{
    "id": 1,
    "userId": 1,
    "title": "sunt aut facere repellat provident occaecati excepturi optio reprehenderit",
    "body": "quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto"
}
*/
const urlUser = "https://ajax.test-danit.com/api/json/users";
const urlPost = "https://ajax.test-danit.com/api/json/posts";

class Card {
  constructor(post, user) {
    this.post = post;
    this.user = user;
    this.element = this.createCard();
  }

  createCard() {
    const card = document.createElement("div");
    card.classList.add("card");
    card.innerHTML = `
        <div class="card__title">
            <img class="icon" src="https://avatar.iran.liara.run/public?v=${this.user.id}" alt="icon" width="40px"/>
            <div class="info">
              <h1 class="name">${this.user.name}</h1>
              <p class="email">${this.user.email}</p>
            </div>
            <button class="delete-btn">DELETE ❌</button>
          </div>
          <div class="card__body">
            <h2 class="post__title">${this.post.title}</h2>
            <p class="post__body">${this.post.body}</p>
          </div>
    `;

    card
      .querySelector(".delete-btn")
      .addEventListener("click", () => this.deleteCard(card));
    return card;
  }

  async deleteCard(card) {
    try {
      const response = await axios.delete(
        `https://ajax.test-danit.com/api/json/posts/${this.post.id}`
      );
      if (response.status === 200) {
        card.remove();
      } else {
        console.error("Error deleting post");
      }
    } catch (error) {
      console.error("Request failed", error);
    }
  }
}

async function fetchData() {
  try {
    const [usersResponse, postsResponse] = await Promise.all([
      axios.get(urlUser),
      axios.get(urlPost),
    ]);

    const usersMap = new Map(usersResponse.data.map((user) => [user.id, user]));
    const container = document.getElementById("container");

    postsResponse.data.forEach((post) => {
      const user = usersMap.get(post.userId);
      if (user) {
        const card = new Card(post, user);
        container.appendChild(card.element);
      }
    });
  } catch (error) {
    console.error("Error fetching data", error);
  }
}

document.addEventListener("DOMContentLoaded", fetchData);
